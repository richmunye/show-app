export const HOME = { link: '/dashboard', name: 'Dashboard' };
export const SIGN_IN = { link: '/', name: 'Sign In' };
export const ABOUT = { link: '/about', name: 'About' };
export const FORGOTPASSWORD = { link: '/reset', name: 'resetPassword' };
export const SETPASSWORD = { link: '/set-password', name: 'setNewPassword' };
