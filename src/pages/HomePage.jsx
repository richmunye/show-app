import React from 'react';
import DashboardLayout from '../components/Home/Home';
import Layout from '../containers/Layout/Layout';

const HomePage = ({ location }) => {
  return (
    <Layout location={location}>
      <DashboardLayout style={{display:"flex"}} />
    </Layout>
  );
};

export default HomePage;
