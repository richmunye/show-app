import React from "react";
import ForgotPassword from "../components/auth/ForgotPassword";

const ResetPage = () => {
  return <ForgotPassword />;
};

export default ResetPage;