import React, { Component } from 'react';
import './Home.scss';
import { Layout,Row,Col } from 'antd';
import LeftSideLayout from '../layouts/leftSideLayout';
import HeaderLayout from '../layouts/headerLayout';
import MainLayout from '../layouts/mainLayout';
import RightLayout from '../layouts/rightSideLayout';



class DashboardLayout extends Component{

  state = {
    collapsed: false,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };
  render(){
    return(
     <Layout style={{display:'flex', minHeight:'100vh'}}>
       <LeftSideLayout/>
    <Layout className="site-layout">
      <HeaderLayout/>
        <Row style={{display:'flex', width:'100%'}}>
        <Col span={18}>
        <MainLayout style={{width:'100%'}}/>
        </Col>
        <Col span={6}>
        <RightLayout/>
        </Col>
      </Row>
      
    </Layout>
  </Layout>
    )
  }
}
export default DashboardLayout;