import React, { Component } from 'react';
import './SignIn.scss';
import { HOME } from '../../constants/routes/';
import { connect } from 'react-redux';
import authenticate from '../../actions/authAction';
import { Redirect } from 'react-router-dom';
import Loader from '../Loader/Loader';
import redIcon from '../../assets/red_icon.png';
// import {Layout, Content} from 'antd';
import { USERNAME_ERROR, PASSWORD_ERROR } from '../../constants/error';
import Layout from 'antd/lib/layout/layout';
import Form from 'antd/lib/form/Form';
import {  Input } from 'antd';
import {Link} from 'react-router-dom'

export const validateForm = (username, password) => {
  
  try {
    if (username == null || username === '') {
      throw new Error(USERNAME_ERROR);
    }
    if (password == null || password === '') {
      throw new Error(PASSWORD_ERROR);
    }
    return true;
  } catch (error) {
    return error.message;
  }
};

class SignIn extends Component {
  state = {
    username: '',
    password: '',
  };
  
  getFormInput = ({ target: { name, value } }) => {
    if (name === 'username' || name === 'password') this.setState({ [name]: value });
  };
  
  signIn = () => {
    // const { username, password } = this.state;
    // const history = useHistory();
    // const { authenticate } = this.props;
    // if (validateForm(username, password)) {
    //   authenticate({ username, password });
    // }
    window.location('/dashboard')
  };

  renderSignInFields = (display, error) => {
    return (
    //   <Layout className="container">
    //     <div className="content-container"></div>
    // </Layout>
      <Layout id="signin-container">
        <div className="clip-container"></div>
        <div className="content">
          <div className="loader"></div>
          <div id="signin-fields">
          <img src={redIcon} alt="logo"/>
          <h2>Organizer login</h2>
          <p>use your company email to login</p>
          <p className="error" style={{ display }}>
            {error}
          </p>
          <Form onSubmit={e => e.preventDefault()}>
            <label>Email</label>
            <input
              type="text"
              name="username"
              placeholder="Email"
              onChange={this.getFormInput}
            />
            <label>Password</label>
            <Input
              type="password"
              name="password"
              placeholder="Password"
              onChange={this.getFormInput}
            />
            <input type="submit" id="sign-in-button" onClick={this.signIn} value="Sign In" />
            <p className="forgot-password">forgot password? <strong><Link className="forgot-link"to="/reset">Reset</Link></strong></p>
          </Form>
        </div>
        </div>
        
        
      </Layout>
    );
  };

  render() {
    const { isAuth, loading, error } = this.props;
    if (isAuth) {
      return <Redirect to={HOME.link} />;
    } else {
      if (loading) {
        return <Loader />;
      } else if (error) {
        return this.renderSignInFields('block', error);
      } else {
        return this.renderSignInFields('none', error);
      }
    }
  }
}

const mapState = ({ authenticate: { isAuth, loading, error } }) => ({
  isAuth,
  loading,
  error,
});

const mapDispatch = dispatch => ({
  authenticate: user => dispatch(authenticate(user)),
});

export default connect(mapState, mapDispatch)(SignIn);
