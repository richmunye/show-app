 import React,{Component} from 'react';
import './SignIn.scss';
import redIcon from '../../assets/red_icon.png';
import { USERNAME_ERROR, PASSWORD_ERROR } from '../../constants/error';
import Layout from 'antd/lib/layout/layout';
import { Redirect } from 'react-router-dom';
import Form from 'antd/lib/form/Form';
import {  Input } from 'antd';
import Loader from '../Loader/Loader';
import { HOME } from '../../constants/routes/';
import {Link} from 'react-router-dom'
// import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';

export const validateForm = (username, password) => {
  try {
    if (username == null || username === '') {
      throw new Error(USERNAME_ERROR);
    }
    if (password == null || password === '') {
      throw new Error(PASSWORD_ERROR);
    }
    return true;
  } catch (error) {
    return error.message;
  }
};
 class SetPassword extends Component{

     state = {
    username: '',
    password: '',
  };

  getFormInput = ({ target: { name, value } }) => {
    if (name === 'username' || name === 'password') this.setState({ [name]: value });
  };

  signIn = () => {
    const { username, password } = this.state;
    const { authenticate } = this.props;
    if (validateForm(username, password)) {
      authenticate({ username, password });
    }
  };
  renderSetPasswordFields=(display,error)=>{
      return(
         <Layout id="signin-container">
        <div className="clip-container"></div>
        <div className="content">
            <div className="loader-forgot"></div>
            <div id="signin-fields">
          <img src={redIcon} alt="logo"/>
          <h2>Set new password</h2>
          <p>Lorem ipsum dolor sit amet, consecteur adipisicing elit.</p>
          <p className="error" style={{ display }}>
            {error}
          </p>
          <Form onSubmit={e => e.preventDefault()}>
              <label>Password</label>
            <Input
              type="password"
              name="password"
              placeholder="password"
              onChange={this.getFormInput}
              required
            />
            <input type="submit" id="sign-in-button" onClick={this.signIn} value="Change Password" />
            <p className="forgot-password">Recall password? <strong><Link className="forgot-link"to="/">Login</Link></strong></p>
          </Form>
        </div>
        </div>
        
      </Layout>
     )
  }
  render() {
    const { isAuth, loading, error } = this.props;
    if (isAuth) {
      return <Redirect to={HOME.link} />;
    } else {
      if (loading) {
        return <Loader />;
      } else if (error) {
        return this.renderSetPasswordFields('block', error);
      } else {
        return this.renderSetPasswordFields('none', error);
      }
    }
  }
     
 }
 
 export default SetPassword;