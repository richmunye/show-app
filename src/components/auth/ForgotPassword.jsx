 import React,{Component} from 'react';
import './SignIn.scss';
import redIcon from '../../assets/red_icon.png';
import { USERNAME_ERROR, PASSWORD_ERROR } from '../../constants/error';
import Layout from 'antd/lib/layout/layout';
import { Redirect } from 'react-router-dom';
import Form from 'antd/lib/form/Form';
import {  Input } from 'antd';
import Loader from '../Loader/Loader';
import { HOME, SETPASSWORD } from '../../constants/routes/';
import {Link} from 'react-router-dom'

export const validateForm = (username, password) => {
  try {
    if (username == null || username === '') {
      throw new Error(USERNAME_ERROR);
    }
    if (password == null || password === '') {
      throw new Error(PASSWORD_ERROR);
    }
    return true;
  } catch (error) {
    return error.message;
  }
};
 class ForgotPassword extends Component{

     state = {
    username: '',
    password: '',
  };

  getFormInput = ({ target: { name, value } }) => {
    if (name === 'username' || name === 'password') this.setState({ [name]: value });
  };

  signIn = () => {
    // const { username, password } = this.state;
    // const { authenticate } = this.props;
    // if (validateForm(username, password)) {
    //   authenticate({ username, password });
    // } 
    return <Redirect to="/set-password"/>
  };
  handleSubmit=(e)=>{
      e.preventDefault();
      return <Redirect to={SETPASSWORD.link}/>
  }
  renderForgotPasswordFields=(display,error)=>{
      return(
         <Layout id="signin-container">
        <div className="clip-container"></div>
        <div className="content">
            <div className="loader-forgot"></div>
            <div id="signin-fields">
          <img src={redIcon} alt="logo"/>
          <h2>Forgot Password</h2>
          <p>use your showApp email to reset your password</p>
          <p className="error" style={{ display }}>
            {error}
          </p>
          <Form onSubmit={this.handleSubmit}>
              <label>Email</label>
            <Input
              type="Email"
              name="Email"
              placeholder="Email"
              onChange={this.getFormInput}
            //   required
            />
            <input type="submit" id="sign-in-button" onClick={this.handleSubmit} value="Submit" />
            <p className="forgot-password">back to login? <strong><Link className="forgot-link"to="/">Login</Link></strong></p>
          </Form>
        </div>
        </div>
        
      </Layout>
     )
  }
  render() {
    const { isAuth, loading, error } = this.props;
    if (isAuth) {
      return <Redirect to={HOME.link} />;
    } else {
      if (loading) {
        return <Loader />;
      } else if (error) {
        return this.renderForgotPasswordFields('block', error);
      } else {
        return this.renderForgotPasswordFields('none', error);
      }
    }
  }
     
 }
 
 export default ForgotPassword;