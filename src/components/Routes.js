import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import SignInPage from '../pages/SignInPage';
import AboutPage from '../pages/AboutPage';
import HomePage from '../pages/HomePage';
import ResetPage from '../pages/ForgotPassword';
import SetPasswordPage from '../pages/SetPassword';
import PageNotFound from './PageNotFound/pageNotFound';

import * as routes from '../constants/routes';

const Routes = ({ isAuth }) => (
  <Router>
    <Switch>
      <Route exact path={routes.ABOUT.link} component={AboutPage} />

      <Route exact path={routes.HOME.link} component={HomePage} />
      <Route exact path={routes.FORGOTPASSWORD.link} component={ResetPage} />
      <Route exact path={routes.SETPASSWORD.link} component={SetPasswordPage} />

      {/* This has a system if a user is logged, can't go to signin again unless he logsout */}
      <Route
        exact
        path={routes.SIGN_IN.link}
        render={(props) =>
          isAuth ? <Redirect to={routes.HOME.link} /> : <SignInPage {...props} />
        }
      />

      <Route exact path="*" component={PageNotFound} />
    </Switch>
  </Router>
);

const mapState = ({ authenticate: { isAuth } }) => ({
  isAuth,
});

export default connect(mapState, null)(Routes);
